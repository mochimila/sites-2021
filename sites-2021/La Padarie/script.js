const modal = 
{
    toggle()
    {
        document
        .querySelector('.modal-overlay')
        .classList.toggle('active')

        document.getElementById('name').value =""
        document.getElementById('n-pao').value =""
    },
    
}



const Quantity = {
    all: [
        {
            id:0,
            name: 'Alexandre Shyjada Sousa',
            totpaes: 50,
            totvalue: 0,
            
        
        },
        
        {
            id:1,
            name: 'Matheus Novais',
            totpaes: 20,
            totvalue: 0,
            
        },
        
        {
            id:2,
            name: 'Victor Peixoto',
            totpaes: 14,
            totvalue: 0,
        },
        ],


    add(transaction){
        Quantity.all.push(transaction)
        App.reload()
    },

    remove(index){
        Quantity.all.splice(index, 1)

        App.reload()

    },

    sumpaes(){

        let npaes = 0;
        Quantity.all.forEach(paessum => {
            npaes += paessum.totpaes
        })

        return npaes

    },

    
}

const DOM = {
    entriesContainer: document.querySelector('#entry-info'),

    addQuantity(transaction, index){
        const div = document.createElement('div')
        div.className = "mb-2";
        div.innerHTML += DOM.innerHTMLQuantity(transaction)
        DOM.entriesContainer.appendChild(div)

        
        
    },

    innerHTMLQuantity(transaction){

        
        const totvalue = utilities.currency_converter(transaction.totpaes/2)
        const totpaes = utilities.pao_calc(transaction.totpaes)


        const html = 
        `
        <div class="fila-card flex justify-between p-4 rounded">
        <div class="flex flex-col">

            <p class="name font-bold">${transaction.name}</p>

            <div class="md:flex">                       
                <div class="flex">
                    <p class="card-font font-bold totalpaes">Total de pães:&nbsp</p>  <p class="card-font">${totpaes} pães &nbsp&nbsp&nbsp&nbsp</p>
                </div>
                <div class="flex">
                    <p class="card-font font-bold totalvalue">Total a pagar:&nbsp<p class="card-font">${totvalue} </p>
                </div>
                
            </div>



        </div> 
            <img src="images/Icontr.svg" onclick="Quantity.remove(${transaction.id})"> 
        </div> 
            `


        return html

    },

    updateCards(){
        document
        .getElementById('qtppl')
        .innerHTML = Quantity.all.length

        document
        .getElementById('qtpaes')
        .innerHTML = Quantity.sumpaes()

        document
        .getElementById('totsum')
        .innerHTML = utilities.currency_converter(Quantity.sumpaes() / 2 )

    },

    clearQuantity()
    {
        DOM.entriesContainer.innerHTML = ""
    }
}

const utilities = {

    currency_converter(value){

        value = value.toLocaleString("pt-BR",{ 
            style: "currency",
            currency: "BRL"
        })

        return value
    },

    pao_calc(value){

        value = Number(value) 

        return value
        
    },



}

const Form = {
    submit(event){
        event.preventDefault()
    }
}

const App = {
    init() {

    

    Quantity.all.forEach(transaction => {
        transaction.id = Quantity.all.indexOf(transaction)
        DOM.addQuantity(transaction)

    })

    DOM.updateCards()
   

    },

    reload() {
        DOM.clearQuantity()
        App.init()

    },
    
}

function addFila(){
    let name = document.getElementById('name')
    let n_pao = document.getElementById('n-pao')

    Quantity.all.push({
            id: Quantity.all.length,
            name: name.value,
            totpaes: Number(n_pao.value),
    })

    modal.toggle()
    App.reload()
}

App.init()









