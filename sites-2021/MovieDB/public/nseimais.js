
    async function toggle(id)
    {
        

        if (id > 0 ){
            

            const response = await fetch ("/apidet/"+id);            
            const data = await response.json();

            
            
            document.getElementById("modal-content").innerHTML = ``
            
            
            var linhadeTag = ""

            if(data.tagline != ""){
              linhadeTag = `<p class='font-bold text-md' >"`+ data.tagline+`"</p>`
            }

            document.getElementById("modal-content").innerHTML += `

            <div>
              <p class="modal-title text-3xl font-bold mb-4" >${data.title} </p>
            </div>

              <img class="rounded-3xl m-2" src="https://image.tmdb.org/t/p/w500${data.poster_path}" style="margin-left:auto; margin-right:auto">
            

          <div>

            
            <div class="flex justify-center m-2 items-center" >
                <p class="modal-title text-lg font-semibold">Título original:</p>
                <p class=" text-md"> &nbsp ${data.original_title} </p> 
                
            </div>
           
            <div>
                ${linhadeTag}
            </div>

            <div class="flex justify-center m-2 items-center" >
              <p class="text-md">Língua original:</p>
              <p class="text-sm"> &nbsp ${data.original_language} </p>
            
            </div>

            <div class="flex text-center m-2 text-justify ">
                <p class=" text-md" >  ${data.overview} </p> <br>
            </div>

              <div class="sinopse" >
                <p class="modal-title text-lg font-semibold" >Voting average:</p>
                <p class="font-semibold" > ${data.vote_average}</p>
              </div>
              
        </div>
       
            `
           

        }
        
        document
        .querySelector('.modal-overlay')
        .classList.toggle('active');
        
    }

    async function getData(pagina) {

        if (typeof pagina === 'undefined') {
            pagina = 1
          }

        const response = await fetch ("/api/"+pagina);
        const data = await response.json();
        console.log(data)
        
        const totalpgs = data.total_pages;

        document.getElementById('aqui').innerHTML = ``

        data.results.forEach((movie) => {

            document.getElementById('aqui').innerHTML +=
             `   
             <div class=" md:w-3/12 w-2/4 flex flex-col items-center my-4 md:justify-between md:h-1/6">
                            
                <img class="maisinfo w-5/6 rounded-lg poster h-5/6 hover:border-yellow-400 my-1" onclick="toggle(${movie.id})" id="${movie.id}" src="https://image.tmdb.org/t/p/w500${movie.poster_path}"   >
                <a class="title text-center">${movie.title}</a>                         
             </div>
                `;
           

            })

        

        var prev = parseFloat(pagina) - 1
        var next = parseFloat(pagina) + 1

        if(pagina == totalpgs){
            document.getElementById("next").innerHTML = `<button class="button-pg flex items-center rounded p-2 font-semibold text-black text-xl text-center invisible" onclick= "getData(`+next+`)" id="botao" >  PROX <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 7l5 5m0 0l-5 5m5-5H6" />
          </svg> </button>`

        }
        else{
            document.getElementById("next").innerHTML = `<button class="button-pg flex items-center rounded p-2 font-semibold text-black text-xl text-center" onclick= "getData(`+next+`)" id="botao" >  NEXT <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 7l5 5m0 0l-5 5m5-5H6" />
          </svg> </button>`
        }
        
        
        if(prev <= 0){
            document.getElementById("prev").innerHTML = `<button class="button-pg flex items-center rounded p-2 font-semibold text-black text-xl text-center invisible" onclick= "getData(`+prev+`)" id="botao" > <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 17l-5-5m0 0l5-5m-5 5h12" />
          </svg> PREV </button>`
        }
        else{
            document.getElementById("prev").innerHTML = `<button class="button-pg flex items-center rounded p-2 font-semibold text-black text-xl text-center" onclick= "getData(`+prev+`)" id="botao" > <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 17l-5-5m0 0l5-5m-5 5h12" />
          </svg> PREV</button>`
        }
        
        
    }



