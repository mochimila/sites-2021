const express = require("express");
const app = express();
const PORT = 3000;
const fetch = require("cross-fetch")

app.use(express.static('public'))
 

app.get("/apidet/:id", async (req, res) => {
  const id = req.params.id;

      
  await fetch ('https://api.themoviedb.org/3/movie/'+id+'?api_key=04c35731a5ee918f014970082a0088b1')
  .then (response => response.json())
  .then (data => res.json(data))
  
 });


app.get("/api", async (req, res) => {
  res.redirect("/api/1")
})


app.get("/api/:page", async (req, res) => {
  const pg = req.params.page;


  await fetch ('https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=04c35731a5ee918f014970082a0088b1&page='+pg)
  .then (response => response.json())
  .then (data => res.json(data))

  
 });



  app.listen(PORT, () => {
    console.log(`Running in http://localhost:${PORT}`);
})



