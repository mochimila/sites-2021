const express = require('express')
const app = express()


app.get('/', function (req, res) {
    res.sendFile(__dirname+'/index.html')
  })

app.get('/:operation/:numero1/:numero2', (req, res) => {
  const operation = req.params.operation;
  const numero1 = parseFloat(req.params.numero1);
  const numero2 = parseFloat(req.params.numero2);
  
  let resultado;
  

  switch(operation)
  {
    case "adicionar":
      resultado = numero1 + numero2;
      break;
    case "subtrair":
      resultado = numero1 - numero2;
      break;
    case "multiplicar":
      resultado = numero1 * numero2;
      break;
    case "dividir":
      resultado = numero1 / numero2;
      break;
  }

  res.json({resultado})

})

  
app.listen(3000)

  //localhost:3000
  //node calculadora.js